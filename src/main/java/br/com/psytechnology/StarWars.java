package br.com.psytechnology;

import br.com.psytechnology.model.Fase;

import javax.swing.*;

/**
 * Hello world!
 */
public class StarWars extends JFrame {

    public static final int SCREEN_RESOLUTION_WIDTH = 1024;
    public static final int SCREEN_RESOLUTION_HEIGHT = 768;

    public StarWars() {
        this.add(new Fase());
        this.setTitle("Star Wars 2");
        this.setSize(SCREEN_RESOLUTION_WIDTH, SCREEN_RESOLUTION_HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new StarWars();
    }
}
