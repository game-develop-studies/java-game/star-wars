package br.com.psytechnology.model;

import javax.swing.*;
import java.awt.*;

public abstract class Ator {
    private int inicioEixoX, inicioEixoY, novoEixoX, novoEixoY, altura, largura;
    private Image image;
    private boolean isVisivel;
    public static int LARGURA = 938;
    public static int VELOCIDADE = 2;

    protected Ator(int inicioEixoX, int inicioEixoY, boolean isVisivel) {
        this.inicioEixoX = inicioEixoX;
        this.inicioEixoY = inicioEixoY;
        this.isVisivel = isVisivel;
    }

    public int getInicioEixoX() {
        return inicioEixoX;
    }

    public void setInicioEixoX(int inicioEixoX) {
        this.inicioEixoX = inicioEixoX;
    }

    public int getInicioEixoY() {
        return inicioEixoY;
    }

    public void setInicioEixoY(int inicioEixoY) {
        this.inicioEixoY = inicioEixoY;
    }

    public int getNovoEixoX() {
        return novoEixoX;
    }

    public void setNovoEixoX(int novoEixoX) {
        this.novoEixoX = novoEixoX;
    }

    public int getNovoEixoY() {
        return novoEixoY;
    }

    public void setNovoEixoY(int novoEixoY) {
        this.novoEixoY = novoEixoY;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public boolean isVisivel() {
        return isVisivel;
    }

    public void setVisivel(boolean visivel) {
        isVisivel = visivel;
    }

    protected void load(String pathImage) {
        setImage(new ImageIcon(pathImage).getImage());
        setLargura(image.getWidth(null));
        setAltura(image.getHeight(null));
    }

    protected abstract void updateMove();

    protected abstract Rectangle limiteColisao();
}
