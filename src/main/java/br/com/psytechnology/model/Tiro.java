package br.com.psytechnology.model;

import java.awt.*;

public class Tiro extends Ator {
    protected Tiro(int inicioEixoX, int inicioEixoY, boolean isVisivel) {
        super(inicioEixoX, inicioEixoY, isVisivel);
    }

    public void loadSprite() {
        super.load("src/main/resources/tiro.png");
    }

    @Override
    protected void updateMove() {
        var novoEixoX = getInicioEixoX() + VELOCIDADE;
        setInicioEixoX(novoEixoX);
        if (getInicioEixoX() > LARGURA) {
            setVisivel(false);
        }
    }

    @Override
    protected Rectangle limiteColisao() {
        return new Rectangle(getInicioEixoX(), getInicioEixoY(), getLargura(), getAltura());
    }


}
