package br.com.psytechnology.model;

import java.awt.*;

public class Enemy1 extends Ator {
    public Enemy1(int playerEixoX, int playerEixoY, boolean isVisivel) {
        super(playerEixoX, playerEixoY, isVisivel);
    }

    public void loadSprite() {
        super.load("src/main/resources/enemy1.png");
    }

    @Override
    protected void updateMove() {
        var novoEixoX = getInicioEixoX() - VELOCIDADE;
        setInicioEixoX(novoEixoX);
        if (getInicioEixoX() < -100) {
            setVisivel(false);
        }
    }

    @Override
    protected Rectangle limiteColisao() {
        return new Rectangle(getInicioEixoX(),getInicioEixoY() + 8,56, 45);
    }

}
