package br.com.psytechnology.model;

import br.com.psytechnology.util.KeyboardInput;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class Player extends Ator {

    private List<Tiro> tiros = new ArrayList();

    protected Player(int inicioEixoX, int inicioEixoY, boolean isVisivel) {
        super(inicioEixoX, inicioEixoY, isVisivel);
    }

    public List<Tiro> getTiros() {
        return tiros;
    }

    public void loadSprite() {
        super.load("src/main/resources/player.png");
    }

    @Override
    protected void updateMove() {
        setInicioEixoX(getInicioEixoX() + getNovoEixoX());
        setInicioEixoY(getInicioEixoY() + getNovoEixoY());
    }

    @Override
    protected Rectangle limiteColisao() {
        return new Rectangle(getInicioEixoX(), getInicioEixoY() + 12, 50, 35);
    }


    public void keyPressed(KeyboardInput keys) {
        if (keys.keyDownOnce(KeyEvent.VK_SPACE)) {
            tiroSimples();
        }
        if (keys.keyDown(KeyEvent.VK_W)) {
            moveUp();
        }
        if (keys.keyDown(KeyEvent.VK_S)) {
            moveDown();
        }
        if (keys.keyDown(KeyEvent.VK_A)) {
            moveLeft();
        }
        if (keys.keyDown(KeyEvent.VK_D)) {
            moveRigth();
        }
    }

    private void tiroSimples() {
        tiros.add(new Tiro(getInicioEixoX() + getLargura() / 2 - 15, getInicioEixoY() + (getAltura() / 2 - 55), true));
    }

    private void moveUp() {
        setNovoEixoY(-3);
    }

    private void moveDown() {
        setNovoEixoY(3);
    }

    private void moveLeft() {
        setNovoEixoX(-3);
    }

    private void moveRigth() {
        setNovoEixoX(3);
    }

    public void stop() {
        setNovoEixoY(0);
        setNovoEixoX(0);
    }
}
