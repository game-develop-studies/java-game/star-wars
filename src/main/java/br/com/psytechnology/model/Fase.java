package br.com.psytechnology.model;


import br.com.psytechnology.util.KeyboardInput;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import static br.com.psytechnology.StarWars.SCREEN_RESOLUTION_HEIGHT;
import static br.com.psytechnology.StarWars.SCREEN_RESOLUTION_WIDTH;

public class Fase extends JPanel implements ActionListener {

    private final Image imageBackground = new ImageIcon("src/main/resources/background.jpg").getImage();
    private static final int QUANTIDADE_INIMIGOS = 40;
    private Player player = new Player(100, 100, true);
    private Timer timer = new Timer(1, this);
    private List<Enemy1> enemies1 = new ArrayList<>();
    private boolean emJogo = true;
    private KeyboardInput keyboardInput = new KeyboardInput();

    public Fase() {
        this.setFocusable(true);
        this.setDoubleBuffered(true);
        player.loadSprite();
        this.addKeyListener(keyboardInput);
        timer.start();
        inicializaInimigo();
    }

    public void inicializaInimigo() {
        for (int i = 0; i < QUANTIDADE_INIMIGOS; i++) {
            int enemyInicioEixoX = (int) (Math.random() * 8000 + 1024);
            int enemyInicioEixoY = (int) (Math.random() * 650 + 40);
            enemies1.add(new Enemy1(enemyInicioEixoX, enemyInicioEixoY, true));
        }
    }

    @Override
    public void paint(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D) graphics;

        if (emJogo) {
            Stroke stroke1 = new BasicStroke(2f);

            graphics2D.setColor(Color.RED);
            graphics2D.setStroke(stroke1);

            graphics2D.drawImage(imageBackground, 0, 0, 1030, 768, null);
            graphics2D.drawImage(player.getImage(), player.getInicioEixoX(), player.getInicioEixoY(), 60, 59, this);
            graphics2D.draw(player.limiteColisao());

            player.getTiros().forEach(tiro -> {
                tiro.loadSprite();
                graphics2D.drawImage(tiro.getImage(), tiro.getInicioEixoX(), tiro.getInicioEixoY(), this);
                graphics2D.draw(tiro.limiteColisao());
            });

            enemies1.forEach(enemy1 -> {
                enemy1.loadSprite();
                graphics2D.drawImage(enemy1.getImage(), enemy1.getInicioEixoX(), enemy1.getInicioEixoY(), 60, 59, this);
                graphics2D.draw(enemy1.limiteColisao());
            });

        } else {
            Image fimJogo = new ImageIcon("src/main/resources/gameover.gif").getImage();
            graphics2D.drawImage(fimJogo, 0, 0, SCREEN_RESOLUTION_WIDTH, SCREEN_RESOLUTION_HEIGHT, null);
        }
        graphics.dispose();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        keyboardInput.poll();
        player.keyPressed(keyboardInput);
        player.updateMove();

        List<Tiro> tiros = player.getTiros();
        for (int i = 0; i < tiros.size(); i++) {
            Tiro tiro = tiros.get(i);
            if (tiro.isVisivel()) {
                tiro.updateMove();
            } else {
                tiros.remove(i);
            }
        }

        for (int i = 0; i < enemies1.size(); i++) {
            var in = enemies1.get(i);
            if (in.isVisivel()) {
                in.updateMove();
            } else {
                enemies1.remove(i);
            }
        }

//        List<Tiro> tiros = player.getTiros();
//        IntStream.range(0, tiros.size()).forEach(index -> {
//            Tiro tiro = tiros.get(index);
//            if (tiro.isVisivel()) {
//                tiro.updateMove();
//            } else {
//                tiros.remove(index);
//            }
//        });
//
//        IntStream.range(0, enemies1.size()).forEach(index -> {
//            Enemy1 enemy1 = enemies1.get(index);
//            if (enemy1.isVisivel()) {
//                enemy1.updateMove();
//            } else {
//                enemies1.get(index);
//            }
//        });

        checarColisoes();
        repaint();
        player.stop();
    }

    public void checarColisoes() {
        Rectangle formaNave = player.limiteColisao();

        enemies1.forEach(enemy1 -> {
            Rectangle enemyBody = enemy1.limiteColisao();
            if (formaNave.intersects(enemyBody)) {
                player.setVisivel(false);
                enemy1.setVisivel(false);
                emJogo = false;
            }
        });

        player.getTiros().forEach(tiro -> {
            Rectangle tiroBody = tiro.limiteColisao();
            enemies1.forEach(enemy1 -> {
                Rectangle enemyBody = enemy1.limiteColisao();
                if (tiroBody.intersects(enemyBody)) {
                    enemy1.setVisivel(false);
                    tiro.setVisivel(false);
                }
            });
        });
    }
}
